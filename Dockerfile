FROM openjdk:8-jdk-alpine
COPY target/clientes-0.0.1-SNAPSHOT.jar clientes.jar
EXPOSE 8084
ENTRYPOINT ["java", "-jar", "/clientes.jar"]