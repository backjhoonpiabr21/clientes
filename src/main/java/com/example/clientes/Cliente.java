package com.example.clientes;

class Cliente {

    private String id;
    private String nombre;
    private String apellido;
    private String ciudad;

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Cliente(String id, String nombre, String apellido, String ciudad) {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setCiudad(ciudad);
            }

    public String getId() {
        return id;
    }


}
