package com.example.clientes;

import com.example.clientes.data.ClienteMongo;
import com.example.clientes.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientesController {

    @Autowired
    private ClienteRepository repository;


    /* Get lista de clientes */
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListado()
    {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<List<ClienteMongo>>(lista, HttpStatus.OK);
    }
    /* Get Cliente by nombre */
    @GetMapping(value = "/clientesbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerClientePorNombre(@PathVariable String nombre)
    {
        List<ClienteMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ClienteMongo>>(resultado, HttpStatus.OK);
    }
    @GetMapping(value="/clientesbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ClienteMongo>> obtenerclientesPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        System.out.println("Estoy en añadir Mongo" + minimo);
        System.out.println("Estoy en añadir Mongo" + maximo);
        List<ClienteMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ClienteMongo>>(resultado, HttpStatus.OK);
    }

    /* Add nuevo Cliente */
    @PostMapping(value = "/clientes", produces="application/json")
    public ResponseEntity<String> addClienteMongo(@RequestBody Cliente ClienteNuevo) {

        System.out.println("Estoy en añadir Mongo");
        System.out.println(ClienteNuevo.getId());
        System.out.println(ClienteNuevo.getNombre());
        System.out.println(ClienteNuevo.getApellido());
        System.out.println(ClienteNuevo.getCiudad());
       // repository.insert(new ClienteMongo());
        repository.insert(new ClienteMongo(ClienteNuevo.getId(),ClienteNuevo.getNombre(),ClienteNuevo.getApellido(),ClienteNuevo.getCiudad()));

        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }
//    @PutMapping("/clientes")
//    public ResponseEntity<String> updateClienteMongo(@RequestBody Cliente cambios) {
//        ResponseEntity<String> resultado = null;
//        try {
//            List<ClienteMongo> ClienteAModificar = (List<ClienteMongo>) repository.findByNombre(cambios.getNombre());
//            System.out.println("Voy a modificar el Cliente" + String.valueOf(cambios.getId()));
//           // System.out.println("Precio actual: " + String.valueOf(ClienteAModificar);
//            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
//           // ClienteAModificar.setNombre(cambios.getNombre());
//           // ClienteAModificar.setPrecio(cambios.getPrecio());
//            repository.save(new ClienteMongo(cambios.getNombre(), cambios.getPrecio()));
//            //      listaclientes.set(cambios.getId(), ClienteAModificar);
//            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        } catch (Exception ex) {
//            resultado = new ResponseEntity<>("No se ha encontrado el Cliente", HttpStatus.NOT_FOUND);
//        }
//        return resultado;
//
//    }
    @PutMapping(value="/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable String id, @RequestBody ClienteMongo ClienteMongo)
    {
        Optional<ClienteMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            ClienteMongo ClienteAModificar = resultado.get();
            ClienteAModificar.nombre = ClienteMongo.nombre;
      //     ClienteAModificar.color = ClienteMongo.color;
            ClienteAModificar.apellido = ClienteMongo.apellido;
        }
        ClienteMongo guardado = repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    @DeleteMapping(value="/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable String id)
    {
        /* Posibilidad de buscar el Cliente, después eliminarlo si existe */
        repository.deleteById(id);
        return new ResponseEntity<String>("Cliente borrado", HttpStatus.OK);
    }
}


