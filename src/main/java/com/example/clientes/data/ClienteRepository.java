package com.example.clientes.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends MongoRepository<ClienteMongo, String> {
    @Query("{'nombre':?0}")
    public List<ClienteMongo> findByNombre(String nombre);
   // public ProductoMongo findByNombre(String nombre);
   @Query("{'precio': {$gt: ?0, $lt: ?1}}")
   public List<ClienteMongo> findByPrecio(double minimo, double maximo);
}
